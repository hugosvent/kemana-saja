package id.mobile.kemanasaja.services;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationRequest;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import id.mobile.kemanasaja.helpers.LocationHelper;
import id.mobile.kemanasaja.helpers.PermissionHelper;
import id.mobile.kemanasaja.models.entities.CheckInLocation;
import io.nlopez.smartlocation.OnReverseGeocodingListener;
import io.nlopez.smartlocation.SmartLocation;
import io.realm.Realm;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;
import rx.Subscription;
import rx.functions.Action1;
import rx.functions.Func1;

import static id.mobile.kemanasaja.views.fragments.MyPlacesFragment.primaryKeyValue;

/**
 * Created by javent on 20/12/16.
 */

public class CheckInService extends Service {
    private final static int ACTIVITY_RECOGNITION_INTERVAL = 600000;
    private final static int MINIMUM_DISTANCE_TO_SEND = 0;

    private final static int LOCATION_NUM_UPDATES = 1;
    private final static int LOCATION_TIMEOUT_SECONDS = 45;

    private long lastActivityRecognitionSpan = 0;
    private int stillCount = 0;
    private int failedCount = 0;

    private ReactiveLocationProvider locationProvider;
    private Subscription activitySubscription, locationSubscription;
    private LocationRequest locationRequest;
    private Action1<ActivityRecognitionResult> recognitionResultAction;

    private Realm realm;

    private CheckInLocation checkInLocation;

    public static void setCheckInService(Context context, boolean isActivated) {
        setAutoCheckInPreference(context, isActivated);

        if (isActivated) {

            startCheckInService(context);
        } else {

            stopCheckInService(context);
        }
    }

    public static void startCheckInService(Context context) {
        if (isLocationServiceAllowed(context)) {
            context.startService(new Intent(context, CheckInService.class));
        }
    }

    public static void stopCheckInService(Context context) {
        context.stopService(new Intent(context, CheckInService.class));
    }

    public static boolean isLocationServiceAllowed(Context context) {
        return getAutoCheckInPreference(context)
                && LocationHelper.isLocationEnabled(context)
                && PermissionHelper.isPermissionGranted(context, PermissionHelper.Permission.LOCATION)
                && isGooglePlayServiceAvailable(context);
    }

    public static boolean isGooglePlayServiceAvailable(Context context) {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        return googleAPI.isGooglePlayServicesAvailable(context) == ConnectionResult.SUCCESS;
    }

    public static boolean getAutoCheckInPreference(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("KemanaSaja", 0);
        return preferences.getBoolean("AutoCheckin", true);
    }

    private static void setAutoCheckInPreference(Context context, boolean on) {
        SharedPreferences preferences = context.getSharedPreferences("KemanaSaja", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("AutoCheckin", on);
        editor.apply();
    }

    public static boolean checkAndRequestGooglePlayService(final Fragment fragment, final int requestCode) {
        if (!isGooglePlayServiceAvailable(fragment.getActivity())) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(fragment.getActivity());

            // set dialog message
            alertDialogBuilder
                    .setMessage("Please <b>enable</b> or <b>update</b> google play services to get recommendations")
                    .setCancelable(false)
                    .setPositiveButton("Updates",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            final String appPackageName = "com.google.android.gms";
                            try {
                                fragment.startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)), requestCode);
                            } catch (android.content.ActivityNotFoundException e) {
                                fragment.startActivityForResult(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)), requestCode);
                            }
                        }
                    })
                    .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();

            return false;
        }

        return true;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        if (activitySubscription != null && activitySubscription.isUnsubscribed()) {
            startActivityRecognition();
        }
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Kemana Saja Auto Check-In Service")
                .setContentText("Service Is Running");

        Notification notification = notificationBuilder.build();

        startForeground(1024, notification);

        locationProvider = new ReactiveLocationProvider(this);
        startActivityRecognition();
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setNumUpdates(LOCATION_NUM_UPDATES)
                .setInterval(5000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopForeground(true);

        if (activitySubscription != null && !activitySubscription.isUnsubscribed()) {
            activitySubscription.unsubscribe();
        }
    }

    private void startActivityRecognition() {

        activitySubscription = locationProvider.getDetectedActivity(ACTIVITY_RECOGNITION_INTERVAL)
                .filter(new Func1<ActivityRecognitionResult, Boolean>() {
                    @Override
                    public Boolean call(ActivityRecognitionResult activityRecognitionResult) {
                        return filterActivityRecognition(activityRecognitionResult);
                    }
                })
                .subscribe(getRecognitionResultAction());
    }

    private Action1<ActivityRecognitionResult> getRecognitionResultAction() {

        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        if (recognitionResultAction == null) {
            recognitionResultAction = new Action1<ActivityRecognitionResult>() {
                @Override
                public void call(ActivityRecognitionResult activityRecognitionResult) {
                    if (activityRecognitionResult != null
                            && isDeviceStill(activityRecognitionResult.getProbableActivities())
//                            && calendar.get(Calendar.HOUR_OF_DAY) > 6
                            && (stillCount == 1)) {
                        getLocation();
                    }
                }
            };
        }

        return recognitionResultAction;
    }

    private synchronized void getLocation() {

        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (locationSubscription != null && !locationSubscription.isUnsubscribed()) {
            locationSubscription.unsubscribe();
        }
        locationSubscription = locationProvider.getUpdatedLocation(locationRequest)
                .timeout(LOCATION_TIMEOUT_SECONDS, TimeUnit.SECONDS, Observable.just((Location) null))
                .subscribe(new Action1<Location>() {
                    @Override
                    public void call(Location location) {
                        checkLocation(CheckInService.this, locationSubscription, location);
                    }
                });
    }

    private synchronized void checkLocation(Context context, Subscription subscription, Location location) {

        if (location != null && location.getAccuracy() <= 50) {
            LocationHelper.saveLocation(location, context);
            if (getSavedCheckInLocation(context) == null || location.distanceTo(getSavedCheckInLocation(context)) >= MINIMUM_DISTANCE_TO_SEND) {
                storeLocation(location);
            }
        } else {

            onLocationFailed();
        }
        subscription.unsubscribe();
    }

    private void storeLocation(final Location location) {
        if (location != null) {

            Realm.init(this);

            realm = Realm.getDefaultInstance();

            SmartLocation.with(this).geocoding()
                    .reverse(location, new OnReverseGeocodingListener() {
                        @Override
                        public void onAddressResolved(Location locationForAddress, final List<Address> list) {
                            realm.executeTransaction(new Realm.Transaction() {
                                @Override
                                public void execute(Realm realm) {

                                    primaryKeyValue = new AtomicLong(realm.where(CheckInLocation.class).max("id") != null ? realm.where(CheckInLocation.class).max("id").longValue() : 0L);
                                    checkInLocation = realm.createObject(CheckInLocation.class, primaryKeyValue.incrementAndGet());

                                    checkInLocation.setCheckInTime(Calendar.getInstance().getTime());

                                    checkInLocation.setAddress(list.get(0).getAddressLine(0));



                                    checkInLocation.setLatitude(Double.toString(location.getLatitude()));
                                    checkInLocation.setLongitude(Double.toString(location.getLongitude()));

                                    saveCheckInLocation(location, CheckInService.this);
                                }
                            });
                        }
                    });
        }
    }

    private void onLocationFailed() {
        failedCount++;
        if (failedCount <= 3) {
            stillCount = 1;
        }
    }

    private boolean filterActivityRecognition(ActivityRecognitionResult activityRecognitionResult) {

        long nowMillis = new Date().getTime();
        boolean isAllowed = nowMillis - lastActivityRecognitionSpan >= ACTIVITY_RECOGNITION_INTERVAL - 15000;
        boolean isDeviceStill = isDeviceStill(activityRecognitionResult.getProbableActivities());
        if (isAllowed) {
            lastActivityRecognitionSpan = nowMillis;
            if (isDeviceStill) {
                stillCount++;
            }
        }
        if (!isDeviceStill) {
            stillCount = 0;
            failedCount = 0;
        }

        return isAllowed;
    }

    private boolean isDeviceStill(List<DetectedActivity> probableActivities) {
        if (probableActivities == null) {
            return false;
        } else {
            for (DetectedActivity activity : probableActivities) {
                switch (activity.getType()) {
                    case DetectedActivity.IN_VEHICLE: {
                        if (activity.getConfidence() >= 30) {
                            return false;
                        }
                        break;
                    }
                    case DetectedActivity.RUNNING: {
                        if (activity.getConfidence() >= 20) {
                            return false;
                        }
                        break;
                    }
                    case DetectedActivity.ON_BICYCLE: {
                        if (activity.getConfidence() >= 20) {
                            return false;
                        }
                        break;
                    }
                }
            }
        }
        return true;
    }

    private void saveCheckInLocation(Location location, Context context) {
        if (null != location && null != context) {
            String lastLocation = location.getLatitude() + "," + location.getLongitude();
            SharedPreferences preferences = context.getSharedPreferences
                    ("KemanaSaja", 0);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("LastCheckInLocation", lastLocation);
            editor.apply();
        }
    }

    private Location getSavedCheckInLocation(Context context) {
        if (null != context) {
            SharedPreferences preferences = context.getSharedPreferences("KemanaSaja", 0);
            if (preferences.contains("LastCheckInLocation")) {
                String[] locationArray = preferences.getString("LastCheckInLocation", "").split(",");
                Double latitude = Double.valueOf(locationArray[0].trim());
                Double longitude = Double.valueOf(locationArray[1].trim());
                Location location = new Location("LastCheckInLocation");
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                return location;
            }
        }
        return null;
    }
}
