package id.mobile.kemanasaja.services;

import android.content.Context;

import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;

import id.mobile.kemanasaja.models.entities.User;
import id.mobile.kemanasaja.models.interfaces.Rest;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * Created by riskteria on 12/22/16.
 */

public class APIClient {

    public static final String BASE_URL        = "http://ks.buburbulan.xyz/";
    public static final String GRANT_TYPE      = "password";
    public static final Integer CLIENT_ID      = 2;
    public static final String CLIENT_SECRET   = "NVryUmasU9HFZZguzcFSmmFAzdotFRD6uLL4k4QS";

    public static Rest getRest(final Context context) {
        Interceptor requestInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                Request.Builder requestBuilder = request.newBuilder();

                if (null != User.getUserToken(context) && !User.getUserToken(context).equals("")) {
                    requestBuilder.addHeader("Authorization", "Bearer " + User.getUserToken(context));
                }
                return chain.proceed(requestBuilder.build());
            }
        };

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .addInterceptor(requestInterceptor)
                .addNetworkInterceptor(new StethoInterceptor())
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(JacksonConverterFactory.create())
                .build();

        final Rest rest = retrofit.create(Rest.class);

        return rest;
    }

    public static String getAccessClient () {
        return "Bearer ";
    }
}
