package id.mobile.kemanasaja.views.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.mobile.kemanasaja.R;
import id.mobile.kemanasaja.helpers.LocationHelper;
import id.mobile.kemanasaja.helpers.PermissionHelper;
import id.mobile.kemanasaja.services.CheckInService;

public class CheckInFragment extends Fragment {

    public static final int LOCATION_REQUEST_CODE = 1;
    public static final int LOCATION_REQUEST_PERMISSION_CODE = 2;
    public static final int GOOGLE_PLAY_SERVICE_REQUEST_CODE = 3;

    @BindView(R.id.check_in_checkbox)
    CheckBox checkBox;

    public CheckInFragment() {
        // Required empty public constructor
    }

    public static CheckInFragment newInstance() {
        CheckInFragment fragment = new CheckInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_check_in, container, false);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        if (CheckInService.getAutoCheckInPreference(this.getContext())) {
            checkBox.setChecked(true);
            turnOnAutoCheckIn();
        } else {
            checkBox.setChecked(false);
            CheckInService.setCheckInService(CheckInFragment.this.getContext(), false);
        }

        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    turnOnAutoCheckIn();
                } else {
                    CheckInService.setCheckInService(CheckInFragment.this.getContext(), false);
                }
            }
        });

    }

    private void turnOnAutoCheckIn() {
        if (!LocationHelper.isLocationEnabled(getContext())) {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this.getContext());

            // set dialog message
            alertDialogBuilder
                    .setMessage("Please enable Location")
                    .setCancelable(false)
                    .setPositiveButton("Settings",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_REQUEST_CODE);
                        }
                    })
                    .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();

            // show it
            alertDialog.show();
        } else if (!PermissionHelper.checkAndRequestPermission(CheckInFragment.this,
                PermissionHelper.Permission.LOCATION,
                "Please enable Location Permission to activate Auto Check-In",
                LOCATION_REQUEST_PERMISSION_CODE)) {

        } else if (!CheckInService.checkAndRequestGooglePlayService(CheckInFragment.this,
                GOOGLE_PLAY_SERVICE_REQUEST_CODE)) {

        } else {
            CheckInService.setCheckInService(getContext(), true);
        }

    }
}
