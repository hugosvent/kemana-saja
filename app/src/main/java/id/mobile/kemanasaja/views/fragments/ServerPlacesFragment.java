package id.mobile.kemanasaja.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SearchView;

import com.borax12.materialdaterangepicker.date.DatePickerDialog;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.mobile.kemanasaja.R;
import id.mobile.kemanasaja.models.entities.CheckInLocation;
import id.mobile.kemanasaja.presenters.LocationAdapter;
import id.mobile.kemanasaja.presenters.LocationPresenter;


public class ServerPlacesFragment extends Fragment implements OnMapReadyCallback, LocationAdapter.OnItemClick {

    private String addressQuery;
    private Calendar beginDate;
    private Calendar endDate;
    GoogleMap map;
    Marker marker;
    LocationPresenter locationPresenter = new LocationPresenter();

    HashMap<String,String> filters;

    @BindView(R.id.map_view)
    MapView mapView;
    @BindView(R.id.location_recycler_view)
    RecyclerView locationRecycler;
    @BindView(R.id.date_range_filter)
    Button dateRangeFilterButton;
    @BindView(R.id.address_search)
    SearchView addressSearch;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    CheckInLocation checkInLocation;
    ArrayList<CheckInLocation> listItem = new ArrayList<>();
    LocationAdapter adapter;
    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");


    public ServerPlacesFragment() {
        // Required empty public constructor
    }


    public static ServerPlacesFragment newInstance() {
        ServerPlacesFragment fragment = new ServerPlacesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        filters = new HashMap<String, String>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_server_places, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        adapter = new LocationAdapter(listItem, getContext(),true);
        adapter.setItemClick(this);
        locationRecycler.setAdapter(adapter);
        final LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(this.getContext());
        locationRecycler.setLayoutManager(recyclerLayoutManager);

        if (beginDate == null || endDate == null) {
            dateRangeFilterButton.setText("Date Range Filter");
        } else {
            dateRangeFilterButton.setText("From " + df.format(beginDate.getTime()) + " To " + df.format(endDate.getTime()));
        }

        dateRangeFilterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth, int yearEnd, int monthOfYearEnd, int dayOfMonthEnd) {
                                beginDate = Calendar.getInstance();
                                endDate = Calendar.getInstance();

                                beginDate.set(year, monthOfYear, dayOfMonth);
                                endDate.set(yearEnd, monthOfYearEnd, dayOfMonthEnd);
                                filters.put("address", addressSearch.getQuery().toString());
                                filters.put("begin", df.format(beginDate.getTime()));
                                filters.put("end", df.format(endDate.getTime()));
                                locationPresenter.getLocation(ServerPlacesFragment.this.getContext(), filters, new LocationPresenter.QueryListener() {
                                    @Override
                                    public void onSuccess(@Nullable CheckInLocation[] checkInLocations) {
                                        super.onSuccess(checkInLocations);

                                        if (checkInLocations != null) {
                                            listItem = new ArrayList<CheckInLocation>(Arrays.asList(checkInLocations));
                                        } else {
                                            listItem = new ArrayList<CheckInLocation>();
                                        }
                                        refreshItems();
                                    }
                                });

                                dateRangeFilterButton.setText(df.format(beginDate.getTime()) + " - " + df.format(endDate.getTime()));
                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dpd.show(ServerPlacesFragment.this.getActivity().getFragmentManager(), "string");
            }
        });

        addressSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                    filters.put("address", query);
                    locationPresenter.getLocation(ServerPlacesFragment.this.getContext(), filters, new LocationPresenter.QueryListener() {
                        @Override
                        public void onSuccess(@Nullable CheckInLocation[] checkInLocations) {
                            super.onSuccess(checkInLocations);

                            listItem = new ArrayList<CheckInLocation>(Arrays.asList(checkInLocations));
                            refreshItems();
                        }
                    });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        // Gets the MapView from the XML layout and creates it
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(this);
    }

    private void refreshItems() {
        // Load items
        // ...
        loadItems();
        // Load complete
        onItemsLoadComplete();
    }

    private void loadItems() {
        adapter = new LocationAdapter(listItem, getContext(), true);
        adapter.notifyDataSetChanged();
        adapter.setItemClick(ServerPlacesFragment.this);
        locationRecycler.setAdapter(adapter);
    }

    private void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        this.map.getUiSettings().setMyLocationButtonEnabled(false);
        this.map.setMyLocationEnabled(true);

        this.marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(43.1, -87.9)));

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(this.getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Updates the checkInLocation and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.1, -87.9), 10);
        this.map.animateCamera(cameraUpdate);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    private boolean checkFocusRec(View view) {
        if (view.isFocused())
            return true;

        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                if (checkFocusRec(viewGroup.getChildAt(i)))
                    return true;
            }
        }
        return false;
    }


    @Override
    public void onItemClicked(int position) {
        CheckInLocation checkInLocation = listItem.get(position);
        LatLng latLng = new LatLng(Double.parseDouble(checkInLocation.getLatitude()), Double.parseDouble(checkInLocation.getLongitude()));

        if (marker != null) {
            marker.remove();
            marker = map.addMarker(new MarkerOptions().position(latLng));
            marker.setTitle(checkInLocation.getAddress());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20f));
        }
//                .position(new LatLng(Double.parseDouble(checkInLocation.getLatitude()), Double.parseDouble(checkInLocation.getLongitude())))
    }
}
