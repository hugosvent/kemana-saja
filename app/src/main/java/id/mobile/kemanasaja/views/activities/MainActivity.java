package id.mobile.kemanasaja.views.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.ncapdevi.fragnav.FragNavController;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import id.mobile.kemanasaja.R;
import id.mobile.kemanasaja.models.entities.User;
import id.mobile.kemanasaja.views.fragments.CheckInFragment;
import id.mobile.kemanasaja.views.fragments.PlacesFragment;
import id.mobile.kemanasaja.views.fragments.ProfileFragment;
import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmSchema;


public class MainActivity extends AppCompatActivity implements FragNavController.RootFragmentListener {

    public static final int INDEX_CHECK_IN = 0;
    public static final int INDEX_PLACES = 1;
    public static final int INDEX_PROFILE = 2;
    FragNavController fragNavController;

    static public void startActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        if(User.getUserToken(this) == null || User.getUserToken(this).isEmpty()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return;
        }

        setContentView(R.layout.activity_main);

        // Initialize Realm
        Realm.init(this);

        RealmConfiguration config = new RealmConfiguration.Builder()
                .schemaVersion(1) // Must be bumped when the schema changes
                .migration(new RealmMigration() {
                    @Override
                    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
                        // DynamicRealm exposes an editable schema
                        RealmSchema schema = realm.getSchema();

                        // Migrate to version 1: Add a new class.
                        // Example:
                        // public Person extends RealmObject {
                        //     private String name;
                        //     private int age;
                        //     // getters and setters left out for brevity
                        // }

                        if (oldVersion == 0) {
                            schema.create("CheckInLocation")
                                    .addField("id", long.class)
                                    .addField("latitude", String.class)
                                    .addField("longitude", String.class)
                                    .addField("createdAt", String.class)
                                    .addField("address", String.class)
                                    .addField("checkInTime", Date.class);
                            oldVersion++;
                        }
                    }
                }) // Migration to run instead of throwing an exception
                .build();

        final List<Fragment> fragments = new ArrayList<>(3);

        fragments.add(CheckInFragment.newInstance());
        fragments.add(PlacesFragment.newInstance());
        fragments.add(ProfileFragment.newInstance());


        fragNavController = new FragNavController(savedInstanceState, getSupportFragmentManager(), R.id.contentContainer, fragments, FragNavController.TAB1);

        BottomBar bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                if (tabId == R.id.tab_check_in) {
                    fragNavController.switchTab(fragNavController.TAB1);
                } else if (tabId == R.id.tab_my_places) {
                    fragNavController.switchTab(fragNavController.TAB2);
                }
                else if (tabId == R.id.tab_profile) {
                    fragNavController.switchTab(fragNavController.TAB3);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public Fragment getRootFragment(int i) {
        switch (i) {
            case INDEX_CHECK_IN:
                return CheckInFragment.newInstance();
            case INDEX_PLACES:
                return PlacesFragment.newInstance();
            case INDEX_PROFILE:
                return ProfileFragment.newInstance();
        }
        throw new IllegalStateException("Need to send an index that we know");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (fragNavController != null) {
            fragNavController.onSaveInstanceState(outState);
        }
    }
}
