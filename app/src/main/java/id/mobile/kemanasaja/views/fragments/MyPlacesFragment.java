package id.mobile.kemanasaja.views.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.mobile.kemanasaja.R;
import id.mobile.kemanasaja.models.entities.CheckInLocation;
import id.mobile.kemanasaja.presenters.LocationAdapter;
import id.mobile.kemanasaja.presenters.LocationPresenter;
import io.realm.RealmQuery;

import static id.mobile.kemanasaja.KemanaSajaApplication.realm;


public class MyPlacesFragment extends Fragment implements OnMapReadyCallback, LocationAdapter.OnItemClick {

    public static AtomicLong primaryKeyValue;

    GoogleMap map;
    Marker marker;

    @BindView(R.id.map_view)
    MapView mapView;
    @BindView(R.id.location_recycler_view)
    RecyclerView locationRecycler;
    @BindView(R.id.send_to_server_button)
    Button sendToServerButton;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    CheckInLocation checkInLocation;
    ArrayList<CheckInLocation> listItem = new ArrayList<>();

    RealmQuery<CheckInLocation> query;


    public MyPlacesFragment() {
        // Required empty public constructor
    }

    public static MyPlacesFragment newInstance() {
        MyPlacesFragment fragment = new MyPlacesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        query = realm.where(CheckInLocation.class);

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_my_places, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        listItem.clear();



        if (!listItem.isEmpty()) {
            this.listItem.addAll(query.findAll());
        } else {
            refreshItems();
        }

        final LocationAdapter adapter = new LocationAdapter(listItem, getContext(), false);
        adapter.setItemClick(this);
        locationRecycler.setAdapter(adapter);
        final LinearLayoutManager recyclerLayoutManager = new LinearLayoutManager(this.getContext());
        locationRecycler.setLayoutManager(recyclerLayoutManager);

        sendToServerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LocationPresenter locationPresenter = new LocationPresenter();

                locationPresenter.sendLocation(getContext(), listItem, new LocationPresenter.QueryListener() {
                    @Override
                    public void onSuccess(@Nullable CheckInLocation[] checkInLocations) {
                        refreshItems();
                    }
                });
//
//                ArrayList<CheckInLocation> tempLocArray = new ArrayList<CheckInLocation>();
//                tempLocArray.addAll(query.findAll());
//                LocationAdapter adapter = new LocationAdapter(tempLocArray, getContext());
//                adapter.notifyDataSetChanged();
//                adapter.setItemClick(MyPlacesFragment.this);
//                listItem = tempLocArray;
//                locationRecycler.setAdapter(adapter);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        // Gets the MapView from the XML layout and creates it
        mapView.onCreate(savedInstanceState);

        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(this);
    }

    private void refreshItems() {
        // Load items
        // ...
        loadItems();
        // Load complete
        onItemsLoadComplete();
    }

    private void loadItems() {
        ArrayList<CheckInLocation> tempLocArray = new ArrayList<CheckInLocation>();
        tempLocArray.addAll(query.findAll());
        LocationAdapter adapter = new LocationAdapter(tempLocArray, getContext(), false);
        adapter.notifyDataSetChanged();
        adapter.setItemClick(MyPlacesFragment.this);
        listItem = tempLocArray;
        locationRecycler.setAdapter(adapter);
    }

    private void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        if (locationRecycler.getAdapter().getItemCount() == 0) {
            sendToServerButton.setVisibility(View.GONE);
        }
        swipeRefreshLayout.setRefreshing(false);
    }

//    private void populateData() {
//        realm.executeTransaction(new Realm.Transaction() {
//            @Override
//            public void execute(Realm realm) {
//
//                primaryKeyValue = new AtomicLong(realm.where(CheckInLocation.class).max("id") != null ? realm.where(CheckInLocation.class).max("id").longValue() : 0L);
//                checkInLocation = realm.createObject(CheckInLocation.class, primaryKeyValue.incrementAndGet());
//                checkInLocation.setAddress("Jalan Jendral Gatot Subroto No. 36" + Math.random() + " , Sei Sikambing D, Medan Petisah, Kota Medan, Sumatera Utara 20118, Indonesia");
//
//                checkInLocation.setCheckInTime(Calendar.getInstance().getTime());
//
//                Random rand = new Random();
//
//                double longitude = 98.672223 + 0.00005 * rand.nextInt(10);
//                double latitude = 3.5952 + 0.00005 * rand.nextInt(10);
//
//
//                checkInLocation.setLatitude(Double.toString(latitude));
//                checkInLocation.setLongitude(Double.toString(longitude));

//            }
//        });
//    }

    @Override
    public void onMapReady(GoogleMap map) {
        this.map = map;
        this.map.getUiSettings().setMyLocationButtonEnabled(false);
        this.map.setMyLocationEnabled(true);

        this.marker = map.addMarker(new MarkerOptions()
                .position(new LatLng(43.1, -87.9)));

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        try {
            MapsInitializer.initialize(this.getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Updates the checkInLocation and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(43.1, -87.9), 10);
        this.map.animateCamera(cameraUpdate);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();

    }

    private boolean checkFocusRec(View view) {
        if (view.isFocused())
            return true;

        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                if (checkFocusRec(viewGroup.getChildAt(i)))
                    return true;
            }
        }
        return false;
    }


    @Override
    public void onItemClicked(int position) {
        CheckInLocation checkInLocation = listItem.get(position);
        LatLng latLng = new LatLng(Double.parseDouble(checkInLocation.getLatitude()), Double.parseDouble(checkInLocation.getLongitude()));

        if (marker != null) {
            marker.remove();
            marker = map.addMarker(new MarkerOptions().position(latLng));
            marker.setTitle(checkInLocation.getAddress());
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 20f));
        }
//                .position(new LatLng(Double.parseDouble(checkInLocation.getLatitude()), Double.parseDouble(checkInLocation.getLongitude())))
    }
}
