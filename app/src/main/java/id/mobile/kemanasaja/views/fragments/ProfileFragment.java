package id.mobile.kemanasaja.views.fragments;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.mobile.kemanasaja.R;
import id.mobile.kemanasaja.models.entities.User;
import id.mobile.kemanasaja.presenters.UserPresenter;
import id.mobile.kemanasaja.views.activities.LoginActivity;

public class ProfileFragment extends Fragment {

    UserPresenter userPresenter = new UserPresenter();

    @BindView(R.id.text_name)
    TextView textName;
    @BindView(R.id.text_address)
    TextView textAddress;
    @BindView(R.id.text_email)
    TextView textEmail;
    @BindView(R.id.text_mobile)
    TextView textMobile;

    @BindView(R.id.input_name)
    EditText inputName;
    @BindView(R.id.input_address)
    EditText inputAddress;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_mobile)
    EditText inputMobile;

    @BindView(R.id.user_profile)
    LinearLayout userProfile;
    @BindView(R.id.update_user_profile)
    LinearLayout updateUserProfile;

    @BindView(R.id.btn_update)
    AppCompatButton btnUpdate;
    @BindView(R.id.btn_logout)
    AppCompatButton btnLogout;

    public ProfileFragment() {
        // Required empty public constructor
    }

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.setUserToken(getContext(), null);
                Intent intent = new Intent(ProfileFragment.this.getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                ProfileFragment.this.getActivity().finish();
            }
        });

        loadProfile();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userProfile.getVisibility() == View.VISIBLE) {
                    btnUpdate.setText("Save Profile");
                    userProfile.setVisibility(View.GONE);
                    updateUserProfile.setVisibility(View.VISIBLE);
                } else {
                    saveProfile();
                }
            }
        });
    }

    private void saveProfile() {
        final AlertDialog alertDialog = new AlertDialog.Builder(getContext()).create();
        alertDialog.setTitle("Loading...");
        alertDialog.setMessage("Saving Profile");
        alertDialog.show();

        HashMap<String, String> userData = new HashMap<>();

        userData.put("name", inputName.getText().toString());
        userData.put("address", inputAddress.getText().toString());
        userData.put("email", inputEmail.getText().toString());
        userData.put("mobile", inputMobile.getText().toString());

        userPresenter.updateUserProfile(getContext(), userData, new UserPresenter.GetUserProfile() {
            @Override
            public void onSuccess(User user) {
                userProfile.setVisibility(View.VISIBLE);
                updateUserProfile.setVisibility(View.GONE);
                btnUpdate.setText("Update Profile");
                loadProfile();
                alertDialog.dismiss();

                Toast.makeText(getContext(), "Profile Successfully updated", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void loadProfile() {
        userPresenter.getUserProfile(getContext(), new UserPresenter.GetUserProfile() {
            @Override
            public void onSuccess(User user) {
                textName.setText(user.getName());
                textAddress.setText(user.getAddress());
                textEmail.setText(user.getEmail());
                textMobile.setText(user.getMobile());

                inputName.setText(user.getName());
                inputAddress.setText(user.getAddress());
                inputEmail.setText(user.getEmail());
                inputMobile.setText(user.getMobile());
            }

            @Override
            public void onFailure(Throwable t) {
                Toast.makeText(getContext(), t.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
