package id.mobile.kemanasaja.presenters;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

import github.nisrulz.recyclerviewhelper.RVHAdapter;
import github.nisrulz.recyclerviewhelper.RVHViewHolder;
import id.mobile.kemanasaja.R;
import id.mobile.kemanasaja.models.entities.CheckInLocation;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by javent on 05/12/16.
 */

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> implements RVHAdapter {

    private List<CheckInLocation> checkInLocations;
    private Context context;
    private OnItemClick itemClick;
    public Realm realm = Realm.getDefaultInstance();
    private boolean isFromServer;

    private SimpleDateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm");

    public LocationAdapter(List<CheckInLocation> checkInLocations, Context context, boolean isFromServer) {
        this.checkInLocations = checkInLocations;
        this.context = context;
        this.isFromServer = isFromServer;
    }

    @Override
    public int getItemCount() {
        return checkInLocations.size();
    }

    @Override
    public void onBindViewHolder(final LocationViewHolder holder, final int position) {
        final CheckInLocation checkInLocation = checkInLocations.get(position);

        holder.address.setText(checkInLocation.getAddress());
        holder.checkedInDate.setText(df.format(checkInLocation.getCheckInTime()));

        holder.locationContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemClick != null) {
                    itemClick.onItemClicked(position);
                }
            }
        });

        if (!isFromServer) {
            holder.buttonViewOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //creating a popup menu
                    PopupMenu popup = new PopupMenu(context, view);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.options_menu);

                    final RealmResults<CheckInLocation> checkInLocationQuery = realm.where(CheckInLocation.class).equalTo("id", checkInLocations.get(position).getId()).findAll();
                    final CheckInLocation checkInLocation = checkInLocationQuery.get(0);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.menu1:
                                    //handle menu1 click

                                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
                                    alertDialog.setTitle("Edit Address");
                                    alertDialog.setMessage("Please input new address");

                                    final EditText input = new EditText(context);
                                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                                            LinearLayout.LayoutParams.MATCH_PARENT,
                                            LinearLayout.LayoutParams.MATCH_PARENT);
                                    input.setLayoutParams(lp);
                                    alertDialog.setView(input);

                                    alertDialog.setPositiveButton("YES",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    realm.executeTransaction(new Realm.Transaction() {
                                                        @Override
                                                        public void execute(Realm realm) {
                                                            checkInLocation.setAddress(input.getText().toString());
                                                            notifyDataSetChanged();
                                                        }
                                                    });
                                                }
                                            });

                                    alertDialog.setNegativeButton("Cancel",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.cancel();
                                                }
                                            });

                                    alertDialog.show();
                                    break;
                                case R.id.menu2:

                                    realm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            checkInLocation.deleteFromRealm(); // Delete and remove object directly
                                        }
                                    });

                                    checkInLocations.remove(position);
                                    notifyItemRemoved(position);
                                    notifyItemRangeChanged(position, checkInLocations.size());

                                    //handle menu2 click
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();

                }
            });
        } else {
            holder.buttonViewOption.setVisibility(View.GONE);
        }
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.item_location, parent, false);

        return new LocationViewHolder(itemView);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        return false;
    }

    @Override
    public void onItemDismiss(int position, int direction) {

    }

    public class LocationViewHolder extends RecyclerView.ViewHolder implements RVHViewHolder{
        protected TextView checkedInDate;
        protected TextView address;
        protected LinearLayout buttonViewOption;
        protected CardView locationContainer;
        public LocationViewHolder(View itemView) {
            super(itemView);
            checkedInDate = (TextView) itemView.findViewById(R.id.location_check_in_date);
            address = (TextView) itemView.findViewById(R.id.location_address);
            locationContainer = (CardView) itemView.findViewById(R.id.location_container);
            buttonViewOption = (LinearLayout) itemView.findViewById(R.id.triple_dot_container);

        }

        @Override
        public void onItemSelected(int actionstate) {

        }

        @Override
        public void onItemClear() {

        }
    }

    public OnItemClick getItemClick() {
        return itemClick;
    }

    public void setItemClick(OnItemClick itemClick) {
        this.itemClick = itemClick;
    }

    public interface OnItemClick {
        public void onItemClicked(int position);
    }
}
