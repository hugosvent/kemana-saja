package id.mobile.kemanasaja.presenters;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

import id.mobile.kemanasaja.models.entities.CheckInLocation;
import id.mobile.kemanasaja.services.APIClient;
import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static id.mobile.kemanasaja.KemanaSajaApplication.realm;

/**
 * Created by javent on 24/12/16.
 */

public class LocationPresenter {

    private QueryListener queryListener;

    public void sendLocation(final Context context, final ArrayList<CheckInLocation> locations, final QueryListener queryListener) {
        this.queryListener = queryListener;
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Loading...");
        alertDialog.setMessage("Sending Location To Server");
        alertDialog.show();

        APIClient.getRest(context).checkIn(locations).enqueue(new Callback<CheckInLocation[]>() {

            @Override
            public void onResponse(Call<CheckInLocation[]> call, Response<CheckInLocation[]> response) {
                if (response.isSuccessful()) {
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            for (CheckInLocation location : locations) {
                                CheckInLocation checkInLocation = realm.where(CheckInLocation.class).equalTo("id",location.getId()).findFirst();
                                checkInLocation.deleteFromRealm();
                            }
                        }
                    });
                    Toast.makeText(context, "Locations successfully send to server", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Something goes wrong while sending location", Toast.LENGTH_LONG).show();
                }
                alertDialog.dismiss();
                queryListener.onSuccess(null);
            }

            @Override
            public void onFailure(Call<CheckInLocation[]> call, Throwable t) {
                Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
                alertDialog.dismiss();
            }
        });
    }

    public void getLocation(final Context context, final HashMap<String,String> filters, final QueryListener QueryListener) {
        this.queryListener = QueryListener;
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Loading...");
        alertDialog.setMessage("Getting location from server");
        alertDialog.show();

        APIClient.getRest(context).getLocation(filters).enqueue(new Callback<CheckInLocation[]>() {
            @Override
            public void onResponse(Call<CheckInLocation[]> call, Response<CheckInLocation[]> response) {
                if (response.isSuccessful()) {
                    Toast.makeText(context, "Query finished", Toast.LENGTH_LONG).show();
                    QueryListener.onSuccess(response.body());
                } else {
                    Toast.makeText(context, "Something goes wrong while querying location", Toast.LENGTH_LONG).show();
                    QueryListener.onSuccess(null);
                }
                alertDialog.dismiss();
            }

            @Override
            public void onFailure(Call<CheckInLocation[]> call, Throwable t) {
                Toast.makeText(context, t.toString(), Toast.LENGTH_LONG).show();
                alertDialog.dismiss();
            }
        });
    }

    public abstract static class QueryListener {
        public void onSuccess(@Nullable CheckInLocation[] checkInLocations){}
    }

}
