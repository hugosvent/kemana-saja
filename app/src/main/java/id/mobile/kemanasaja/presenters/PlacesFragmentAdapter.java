package id.mobile.kemanasaja.presenters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;

import id.mobile.kemanasaja.views.fragments.MyPlacesFragment;
import id.mobile.kemanasaja.views.fragments.ServerPlacesFragment;

public class PlacesFragmentAdapter extends FragmentStatePagerAdapter {

    public PlacesFragmentAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:  return MyPlacesFragment.newInstance();
            case 1:  return ServerPlacesFragment.newInstance();
            default: return null;
        }
    }
    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle (int position) {
        if (position == 0) {
            return "My Places";
        } else {
            return "Places on server";
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return PagerAdapter.POSITION_NONE;
    }
}
