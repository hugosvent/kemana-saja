package id.mobile.kemanasaja.presenters;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.util.HashMap;

import id.mobile.kemanasaja.models.entities.AccessToken;
import id.mobile.kemanasaja.models.entities.User;
import id.mobile.kemanasaja.services.APIClient;
import id.mobile.kemanasaja.views.activities.LoginActivity;
import id.mobile.kemanasaja.views.activities.MainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by riskteria on 12/22/16.
 */

public class UserPresenter {

    User user;
    GetUserProfile getUserProfile;

    public void doRegister (final Context context, String name, String address, String email, String mobile, String password) {
        HashMap<String, String> userData = new HashMap<>();

        userData.put("name", name);
        userData.put("address", address);
        userData.put("email", email);
        userData.put("password", password);
        userData.put("mobile", mobile);

        APIClient.getRest(context).register(userData).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                Intent intent = new Intent(context, LoginActivity.class);
                context.startActivity(intent);

                Log.d("info", "bisa");
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("info", "gagal" + t);
            }
        });

    }

    public void getUserProfile(final Context context, final GetUserProfile getUserProfile) {
        this.getUserProfile = getUserProfile;
        APIClient.getRest(context).getUserProfile().enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                getUserProfile.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {

            }
        });
    }

    public void updateUserProfile(final Context context, HashMap<String, String> userData , final GetUserProfile getUserProfile) {
        this.getUserProfile = getUserProfile;
        APIClient.getRest(context).updateUserProfile(userData).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                getUserProfile.onSuccess(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                getUserProfile.onFailure(t);
            }
        });
    }

    public interface GetUserProfile {
        public void onSuccess(User user);
        public void onFailure(Throwable t);
    }

    public void doLogin (final Context context, String username, String password, final ProgressDialog progressDialog) {
        HashMap<String, String> userData = new HashMap<>();

        userData.put("username", username);
        userData.put("password", password);
        userData.put("client_id", APIClient.CLIENT_ID.toString());
        userData.put("client_secret", APIClient.CLIENT_SECRET);
        userData.put("grant_type", APIClient.GRANT_TYPE);

        Call<AccessToken> call = APIClient.getRest(context).login(userData);

        call.enqueue(new Callback<AccessToken>() {
            @Override
            public void onResponse(Call<AccessToken> call, Response<AccessToken> response) {
                if (response.isSuccessful()) {
                    String accessToken = response.body().getAccessToken();

                    User.setUserToken(context, accessToken);

                    MainActivity.startActivity(context);

                    Log.d("data", "Bisa om");
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Wrong login details", Toast.LENGTH_LONG).show();
//                    MainActivity.startActivity(context);
                }

            }

            @Override
            public void onFailure(Call<AccessToken> call, Throwable t) {
                Log.d("data", "error = " + t);
            }
        });

    }
}
