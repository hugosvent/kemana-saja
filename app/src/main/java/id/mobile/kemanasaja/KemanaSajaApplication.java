package id.mobile.kemanasaja;

import android.app.Application;

import com.facebook.stetho.Stetho;

import io.realm.Realm;

public class KemanaSajaApplication extends Application {

    public static Realm realm;

    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        realm = Realm.getDefaultInstance();
        Stetho.initialize(Stetho.newInitializerBuilder(this)
            .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
            .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
            .build());
    }
}
