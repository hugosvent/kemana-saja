package id.mobile.kemanasaja.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;

/**
 * Created by javent on 20/12/16.
 */

public class LocationHelper {

    static public double getDistance(Location from, Context context) {
        return getDistance(from, getSavedLocation(context));
    }

    static public double getDistance(Location from, Location to) {
        if (null != from && null != to) {
            return from.distanceTo(to);
        }

        return 0;
    }

    static public boolean isLocationEnabled(Context context) {
        if (null != context) {
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            try {
                return lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || lm.isProviderEnabled
                        (LocationManager.NETWORK_PROVIDER);
            } catch (Exception ex) {
                return false;
            }
        }
        return false;
    }

    static public void saveLocation(Location location, Context context) {
        if (null != location && null != context) {
            String lastLocation = location.getLatitude() + "," + location.getLongitude();
            SharedPreferences preferences = context.getSharedPreferences
                    ("KemanaSaja", 0);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("LastLocation", lastLocation);
            editor.apply();
        }
    }

    static public Location getSavedLocation(Context context) {
        if (null != context) {
            SharedPreferences preferences = context.getSharedPreferences
                    ("KemanaSaja", 0);
            if (preferences.contains("LastLocation")) {
                String[] locationArray = preferences.getString("LastLocation", "").split(",");
                Double latitude = Double.valueOf(locationArray[0].trim());
                Double longitude = Double.valueOf(locationArray[1].trim());
                Location location = new Location("LastLocation");
                location.setLatitude(latitude);
                location.setLongitude(longitude);
                return location;
            }
        }
        return null;
    }
}
