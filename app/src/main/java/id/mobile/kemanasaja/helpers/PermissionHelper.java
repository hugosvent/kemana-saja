package id.mobile.kemanasaja.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

/**
 * Created by javent on 20/12/16.
 */

public class PermissionHelper {

    public enum Permission {
        LOCATION(android.Manifest.permission.ACCESS_FINE_LOCATION);

        private String permission;


        Permission(String permission) {
            this.permission = permission;
        }

        public String getPermission() {
            return permission;
        }
    }

    public static boolean isPermissionGranted(Context context, Permission permission) {
        return ContextCompat.checkSelfPermission(context, permission.getPermission()) == PackageManager.PERMISSION_GRANTED;
    }

    public static boolean checkAndRequestPermission(final Activity activity, Permission permission, CharSequence message, int requestCode) {
        if (!isPermissionGranted(activity, permission)) {
            //This will be executed if user check the Never ask Again in Permission Request
            if (!ActivityCompat.shouldShowRequestPermissionRationale(activity, permission.getPermission())) {
                showDialogToPermissionSetting(activity, message, requestCode);
                return false;
            }

            // Ask for Permission
            String[] perm = {permission.getPermission()};
            ActivityCompat.requestPermissions(activity, perm, requestCode);
            return false;
        }
        return true;
    }

    public static boolean checkAndRequestPermission(final Fragment fragment, Permission permission, CharSequence message, int requestCode) {
        if (!isPermissionGranted(fragment.getContext(), permission)) {
            // This will be executed if user check the Never ask Again in Permission Request
            if (!fragment.shouldShowRequestPermissionRationale(permission.getPermission())) {
                showDialogToPermissionSetting(fragment, message, requestCode);
                return false;
            }

            // Ask for Permission
            String[] perm = {permission.getPermission()};
            fragment.requestPermissions(perm, requestCode);
            return false;
        }
        return true;
    }

    private static void showDialogToPermissionSetting(final Activity activity, CharSequence message, final int requestCode) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Settings",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + activity.getPackageName())), requestCode);
                    }
                })
                .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    private static void showDialogToPermissionSetting(final Fragment fragment, CharSequence message, final int requestCode) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(fragment.getActivity());

        // set dialog message
        alertDialogBuilder
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Settings",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        fragment.startActivityForResult(new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.parse("package:" + fragment.getActivity().getPackageName())), requestCode);
                    }
                })
                .setNegativeButton("Close",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

}
