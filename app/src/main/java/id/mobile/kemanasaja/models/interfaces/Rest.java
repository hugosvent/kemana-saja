package id.mobile.kemanasaja.models.interfaces;

import java.util.ArrayList;
import java.util.Map;

import id.mobile.kemanasaja.models.entities.AccessToken;
import id.mobile.kemanasaja.models.entities.CheckInLocation;
import id.mobile.kemanasaja.models.entities.User;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.QueryMap;

/**
 * Created by riskteria on 11/4/16.
 */

public interface Rest {

    @Headers("Accept: application/json")
    @POST("oauth/token")
    Call<AccessToken> login(@Body Map userData);

    @Headers("Accept: application/json")
    @POST("register")
    Call<User> register (@Body Map userData);

    @GET("api/v1/profile")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    Call<User> getUserProfile();

    @PUT("api/v1/profile")
    Call<User> updateUserProfile(@Body Map userData);

    @Headers("Accept: application/json")

    @POST("api/v1/location")
    Call<CheckInLocation[]> checkIn (@Body ArrayList<CheckInLocation> locations);

    @Headers("Accept: application/json")
    @GET("api/v1/location")
    Call<CheckInLocation[]> getLocation (@QueryMap Map<String,String> filters);
}
