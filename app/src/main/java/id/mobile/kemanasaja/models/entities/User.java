package id.mobile.kemanasaja.models.entities;

import android.content.Context;
import android.content.SharedPreferences;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by riskteria on 11/4/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    private String name;
    private String address;
    private String email;
    private String mobile;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName ()
    {
        return this.name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getAddress ()
    {
        return this.address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getEmail ()
    {
        return this.email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getMobile ()
    {
        return this.mobile;
    }

    public void setMobile (String mobile)
    {
        this.mobile = mobile;
    }

    public static String getUserToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("KemanaSaja", 0);
        return preferences.getString("AccessToken", null);
    }

    public static void setUserToken(Context context, String token) {
        SharedPreferences preferences = context.getSharedPreferences("KemanaSaja", 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("AccessToken", token);
        editor.apply();
    }

}
